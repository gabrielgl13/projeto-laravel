<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Cliente;
use app\User;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //$this->registerPolicies();

        Gate::define('edit-cliente', function(User $user, Cliente $cliente){
            return $user->id == $cliente->user_id;
        });

        return "espertinho..";

    }
}
