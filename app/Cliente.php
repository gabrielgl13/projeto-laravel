<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Cliente extends Model
{
    protected $fillable = [
        'name', 'email', 'cpf', 'date_birth', 'active', 'user_id', 'image'
    ];

    public function setDateBirthAttribute($value)
    {
        $this->attributes['date_birth'] = date('Y-m-d', strtotime($value));
    }

    public function setImageAttribute($value)
    {
        if ($value){
            $nameToDifference = Str::before($this->attributes['email'], '.com');
            $nameOldFile = Str::before($value->getClientOriginalName(), '.');
            $nameImage = Str::kebab($nameOldFile . '-' . $nameToDifference . "." . $value->extension());
            
            $this->attributes['image'] = $nameImage;

            $upload = $value->storeAs('clientes', $nameImage);
        }        
    }

    public function User()
    {
        $this->belongsTo('User');
    }
}
