<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ClienteRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Cliente;
use Illuminate\Support\Facades\Auth;

class ClienteController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::where('user_id', Auth::user()->id)->paginate(2);
        return view('cliente.index', compact('clientes'));
    }

    public function filter(Request $request)
    {
        $dataForm = $request->except('_token');

        $clientes = Cliente::where(function ($query) use ($request){
                            if ($request['name'])
                                    $query->where('name', 'LIKE', '%'. $request['name']. '%');
                            if ($request['email'])
                                    $query->where('email', 'LIKE', '%'. $request['email']. '%');
                            if ($request['cpf'])
                                    $query->where('cpf', 'LIKE', '%'. $request['cpf']. '%');
                            if ($request['date_birth'])
                                    $query->where('date_birth', $request['date_birth']);
                            if ($request['active'] != 'selecione...')
                                    $query->where('active', $request['active']);
                    })
                    ->where('user_id', Auth::user()->id)
                    ->paginate(2);

        return view('cliente.index', compact('clientes', 'dataForm'));                  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Cliente $cliente)
    {
        return view('cliente.edit', compact('cliente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteRequest $request)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid()){
            $cliente = Cliente::create($request->all() + 
                                        ['user_id' => Auth::user()->id] +
                                        ['image' => $request->image]
                                    );
        } else {
            $cliente = Cliente::create($request->all() + ['user_id' => Auth::user()->id]);       
        }
        
        if ($cliente)
            return  redirect('home')
                        ->with('store', 'Cliente cadastrado com sucesso!');
            
            return redirect()
                       ->back()
                       ->with('error', 'Falha ao inserir');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::find($id);
        return view('cliente.show', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        $this->authorize('edit-cliente', $cliente);

        return view('cliente.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteRequest $request, $id)
    {
        $cliente = Cliente::find($id);

        if ($request->hasFile('image') && $request->file('image')->isValid()){
            
            if ($cliente->image)
                Storage::delete("clientes/{$cliente->image}");

                $cliente->update($request->all() + 
                                    ['user_id' => Auth::user()->id] +
                                    ['image' => $cliente->image]
                                );
             
        } else {
            $cliente->update($request->all() + ['user_id' => Auth::user()->id]);
        }

        if ($cliente)
            return redirect()
                        ->route('cliente.index')
                        ->with('success', 'Cliente editado com sucesso!');
            
            return redirect()
                        ->back()
                        ->with('error', 'Falha ao inserir');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::find($id);

        if ($cliente->image)
            Storage::delete("clientes/{$cliente->image}");

        $cliente->delete();
        return redirect()->back();
    }
}
