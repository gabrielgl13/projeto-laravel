<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rule = $request->route("cliente") ? ",".$request->route("cliente") : "";
        return [
            'name' => 'required|min:2:max:30',
            'email' => 'required|email|min:15|max:50|unique:clientes,email'. $rule,
            'cpf' => 'required|string|max:14',
            'date_birth' => 'required|date',
            'active' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.min' => 'Nome muito curto!',
            'name.max' => 'Nome muito longo',
            'email.min' => 'E-mail Curto!',
            'email.max' => 'E-mail Longo!',
            'cpf.max' => 'CPF muito longo',
            'required' => 'Você não informou um atributo obrigatório!',
            'email' => 'Você deve informar um email válido',
            'date' => ':attribute deve ser uma data',
        ];
    }
}
