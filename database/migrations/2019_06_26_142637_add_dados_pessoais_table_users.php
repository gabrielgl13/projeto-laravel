<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDadosPessoaisTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('cpf')
                    ->unique() 
                    ->nullable() 
                    ->after('name');
            
            $table->date('date_birth') 
                    ->nullable() 
                    ->after('password');
    
            $table->string('image', 260) 
                    ->nullable() 
                    ->after('date_birth');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('cpf');
            $table->dropColumn('date_birth');
            $table->dropColumn('image');
        });
    }
}
