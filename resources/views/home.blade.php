@extends('layouts.app')

@section('content')

@if(session('store'))
    <div class="alert alert-success"> 
        {{ session('store') }}
    </div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <a href="{{ route('cliente.index') }}" class="btn btn-warning ml-2 float-right">Gerenciar Clientes</a>
        <a href="{{ route('cliente.create') }}" class="btn btn-success float-right">Cadastrar Cliente</a>       
        </div>
    </div>
</div>
@endsection
