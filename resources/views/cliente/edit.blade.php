@extends('template')

@section('title', 'Formulário')

@section('content')

@include('helpers.validate_errors')

@if ($cliente->id)
  <nav class="navbar navbar-expand-lg navbar-light bg-dark">
      <a class="navbar-brand" href="{{ route('home') }}" style="color: white">Home</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('cliente.index') }}" style="color: white">Clientes</a>
              </li>
          </ul>
      </div>
  </nav>

  @if(session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
  @endif

  <h1 class="text-center mt-3">Editar Cliente</h1>
@else

  <nav class="navbar navbar-light bg-dark">
      <a class="navbar-brand" href="{{ route('home') }}" style="color: white">Home</a>
  </nav>

  @if(session('error'))
      <div class="alert alert-danger">
          {{ session('error') }}
      </div>
  @endif

  <h1 class="text-center mt-3">Cadastrar Cliente</h1>
@endif

<div class="container">
  @if ($cliente->id)
      <form method="post" action="{{ route('cliente.update', $cliente->id) }}" enctype="multipart/form-data">  
      <input type="hidden" name="_method" value="PUT">  
  @else    
      <form method="post" action="{{ route('cliente.store') }}" enctype="multipart/form-data">
  @endif
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      
      <div class="form-group">
        <label for="name">Nome</label>
        <input type="text" name="name" class="form-control" id="name" placeholder="João" value="{{ old('name', $cliente->name) }}">
      </div>
      
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" class="form-control" id="email" placeholder="name@example.com" value="{{ old('email', $cliente->email) }}">
      </div>
      
      <div class="form-group">
        <label for="cpf">CPF</label>
        <input type="cpf" name="cpf" class="form-control" id="cpf" placeholder="00000000000" value="{{ old('cpf', $cliente->cpf) }}">
      </div>

      <div class="form-group">
          <div class='input-group date'>
              <label for="datepicker">Data de nascimento</label>
              <input type="text" name="date_birth" class="form-control" id="datepicker" placeholder="26/10/1999" value="{{ old('date_birth', $cliente->date_birth) }}"/>          
          </div>
      </div>

      <label class="my-1 mr-2" for="active">Ativo</label>
      <select class="custom-select my-1 mr-sm-2" name="active" id="active">
      <option>Escolha...</option>
        @if (old("active", $cliente->active) == 'S')
            <option value="S" selected>Sim</option>
            <option value="N">Não</option>
        @elseif (old("active", $cliente->active) == 'N')
            <option value="S">Sim</option>
            <option value="N" selected>Não</option>
        @else
            <option value="S">Sim</option>
            <option value="N">Não</option>
        
        @endif
        <!-- prettier -->
      </select>
      
      @if ($cliente->image)
        <label>Foto antiga: </label>
        <div>
        <img src="{{ url('/storage/clientes/' . $cliente->image) }}" alt="{{ $cliente->name }}" style="height: 100px; width: 100px;"><br>
        </div>
      @endif

      <label class="my-2 mr-2" for="active">Foto</label>
      <div class="custom-file">
          <input type="file" class="custom-file-input" name="image" id="customFile">
          <label class="custom-file-label" for="customFile">Selecione uma imagem</label>
      </div>

     <button class="btn btn-primary btn-lg btn-block font-weight-bold mb-2 mt-4" type="submit">
        
        @if ($cliente->id)  
          Editar Cliente
        @else
          Cadastrar Cliente
        @endif
     </button>
  </form>
</div>

@endsection