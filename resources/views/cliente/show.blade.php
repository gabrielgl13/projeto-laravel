@extends('template')

@section('title', 'Mostrar Cliente')

@section('content')

    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <a class="navbar-brand" href="{{ route('home') }}" style="color: white">Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('cliente.index') }}" style="color: white">Clientes</a>
                </li>
            </ul>
        </div>
    </nav>

    <h1 class="text-center mt-3">Dados do Cliente</h1>

    <div class="row">
        <div class="container m-4">
            @if ($cliente->image)
                <img src="{{ url('/storage/clientes/' . $cliente->image) }}" alt="{{ $cliente->name }}" style="height: 100px; width: 100px;"><br>
            @endif    
            <b>Nome: </b> {{ $cliente->name }} <br>
            <b>E-mail: </b> {{ $cliente->email }} <br>
            <b>CPF: </b> {{ $cliente->cpf }} <br>
            <b>Data de Nascimento</b> {{ $cliente->date_birth }} <br>
            @if ( $cliente->active == 'S')
                <b>Ativo: </b> Sim <br>
            @elseif ($cliente->active == 'N')
                <b>Ativo: </b> Não <br>
            @endif            
        </div>
    </div>
    
@endsection