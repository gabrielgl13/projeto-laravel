@extends('template')

@section('title', 'Criar Usuario')

@section('content')

@include('helpers.validate_errors')

<h1 class="text-center">Cadastrar Cliente</h1>

<div class="container">
  <form method="post" action="{{ route('cliente.store') }}">    
      
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      
      <div class="form-group">
        <label for="name">Nome</label>
        <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" placeholder="João">
      </div>
      
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" class="form-control" id="email" value="{{ old('email') }}" placeholder="name@example.com">
      </div>
      
      <div class="form-group">
        <label for="cpf">CPF</label>
        <input type="cpf" name="cpf" class="form-control" id="cpf" value="{{ old('cpf') }}" placeholder="00000000000">
      </div>

      <div class="form-group">
          <div class='input-group date'>
              <label for="datepicker">Data de nascimento</label>
              <input type="date" name="date_birth" class="form-control" id="datepicker" value="{{ old('date_birth') }}" placeholder="26/10/1999"/>          
          </div>
      </div>

      <label class="my-1 mr-2" for="active">Ativo</label>
      <select class="custom-select my-1 mr-sm-2" name="active" id="active">
          <option selected>Escolha...</option>
          <option value="S" {{ old('active') == 'S' ? 'selected' : '' }}>Sim</option>
          <option value="N" {{ old('active') == 'N' ? 'selected' : '' }}>Não</option>
    <!-- prettier -->
      </select>

     <button class="btn btn-primary btn-lg btn-block font-weight-bold mb-2 mt-4" type="submit">Cadastrar Cliente</button>
  </form>
</div>


@endsection