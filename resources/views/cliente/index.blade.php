@extends('template')

@section('title', 'Listar Usuarios')

@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
    
    <nav class="navbar navbar-light bg-dark">
        <a class="navbar-brand" href="{{ route('home') }}" style="color: white">Home</a>
    </nav>

    <h1 class="text-center mt-3">Clientes Cadastrados</h1>

    <div class="row">
        <div class="container mt-4">
            <form method="GET" action="{{ route('filter') }}">
                <div class="form-row">
                    <div class="col-sm-2">
                        <input type="text" name="name" class="form-control" placeholder="Nome">
                    </div>
                    <div class="col-sm-2">
                        <input type="email" name="email" class="form-control" placeholder="email">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" name="cpf" class="form-control" placeholder="CPF">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" name="date_birth" class="form-control" placeholder="Data de Nascimento">
                    </div>
                    <div class="col-sm-2">
                        <select class="custom-select" name="active" id="active">
                            <option selected>selecione...</option>
                            <option value="S">Sim</option>
                            <option value="N">Não</option>
                            <!-- prettier -->
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary">Filtrar Cliente</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="container">
        <table class="table table-bordered mt-4">
            <thead>
                <tr>
                <th scope="col">Nome</th>
                <th scope="col">Email</th>
                <th scope="col">CPF</th>
                <th scope="col">Data de Nascimento</th>
                <th scope="col">Ativo</th>
                <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clientes as $cliente)
                    <tr>
                        <td>{{ $cliente->name }}</td>
                        <td>{{ $cliente->email }}</td>
                        <td>{{ $cliente->cpf }}</td>
                        <td>{{ $cliente->date_birth }}</td>
                        <td>{{ $cliente->active }}</td>
                        <td>
                            <a href="{{ route('cliente.show', $cliente->id) }}" class="btn btn-primary ml-1">Visualizar</a>
                            <button type="button" class="btn btn-danger ml-1" data-toggle="modal" data-target="#modalDelete{{$cliente->id}}">
                                Remover
                            </button>
                            <a href="{{ route('cliente.edit', $cliente->id) }}" class="btn btn-success ml-1">Editar</a>       
                        </td>
                    </tr>

                    <div class="modal fade" id="modalDelete{{$cliente->id}}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalLabel">Remover Cliente</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img src="https://images.emojiterra.com/twitter/512px/2757.png" class="img-fluid img-thumbnail rounded mx-auto d-block" style="height: 10rem;"/>
                                    <p class="text-center mt-4 h5">Você tem certeza que deseja excluir o cliente {{ $cliente->name }}?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                    <form method="post" action="{{ route( 'cliente.destroy', $cliente->id ) }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-primary">Sim, tenho</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>  

                @endforeach
            </tbody>
            </table>
        </div>
    </div>

    <div class="container">
        @if (isset($dataForm))
            {!! $clientes->appends($dataForm)->links() !!}
        @else
            {!! $clientes->links("pagination::bootstrap-4") !!}
        @endif
    </div>

@endsection